<?php 

$dom = new DOMDocument();
$dom->loadHTMLFile("index.html");
$images = $dom->getElementsByTagName('img');
$wrapperBlank = $dom->createElement('a');

foreach ($images as $image) {
	$imgPath = $image->getAttribute('src');
	$wrapper = $wrapperBlank->cloneNode();
	$wrapper->setAttribute('target', '_blank');
	$wrapper->setAttribute('href', $imgPath);
	$image->parentNode->replaceChild($wrapper, $image);
	$wrapper->appendChild($image);
}

echo $dom->saveHTML();
