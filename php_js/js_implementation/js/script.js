HTMLElement.prototype.wrap = function(wrapper){
  this.parentNode.insertBefore(wrapper, this);
  wrapper.appendChild(this);
}

for(var i = 0; i< document.images.length; i++){
	let img = document.images[i];
	let wrapper = document.createElement('a');
	wrapper.setAttribute('target', '_blank');
	wrapper.href = img.src;
	img.wrap(wrapper, img);
}