<?php 

// также можно использовать $_SERVER['DOCUMENT_ROOT']
$folderPath = realpath(__DIR__ . "/../demo.dev");
$fileName = "/archive.zip";
$filePath = $folderPath . $fileName;
try {
	if(file_exists($filePath)) {
		// http заголовки для скачиваемого архива
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=\"" . $fileName. "\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . filesize($filePath));
		header('Connection: close'); 
		ob_end_flush();
		$handle = fopen($filePath, "r");	
		if(!$handle){
			// если не получилось открыть файл
			header ("HTTP/1.0 505 Internal server error");
			throw new Exception("Sorry, can't read the file");
		}
		readfile($filePath);
		exit;
	} else {
		// если файл не найден
		header ("HTTP/1.0 404 Not Found");
		throw new Exception("Sorry, file not found");
	}
} catch (Exception $e) {
	echo $e->getMessage();
}