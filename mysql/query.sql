﻿-- решил привести два решения, 
-- причем несмотря на наличие во втором вложеного запроса, двух условий и JOIN'а
-- он в среднем отрабатывал в три раза быстрее
-- проверял на 1 000 012 записях , функция для заполнения тестовыми данными ниже
SELECT
  c.name
FROM clients c
  JOIN orders o
    ON c.id = o.client
WHERE o.status = 'Complete'
GROUP BY o.client
HAVING COUNT(*) > 4

SELECT
  c.NAME
FROM (SELECT
    client,   
    COUNT(*) cc
  FROM orders o
  WHERE o.STATUS = 'Complete'
  GROUP BY client) AS ords
  JOIN clients c
    ON c.ID = ords.client
WHERE ords.cc > 4

-- функция для заполнения таблицы тестовыми данными
CREATE PROCEDURE insertfake()
BEGIN
DECLARE i INT DEFAULT 1;
DECLARE cli INT;
WHILE i < 1000000 DO
  SET cli = ROUND((RAND() * (1000 - 1)) + 1);
INSERT INTO `orders` (client, `status`)	
  VALUES (cli, (SELECT ELT(ROUND((RAND() * (3 - 1)) + 1), 'Pending', 'Complete', 'Canceled')));

INSERT INTO clients (id, `name`)
  VALUES (cli, cli) ON DUPLICATE KEY UPDATE id=cli;	
    SET i = i + 1;
  END WHILE;
END

CALL insertfake();
 